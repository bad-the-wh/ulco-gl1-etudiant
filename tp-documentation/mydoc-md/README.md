# Mydoc.md

## License

This project is under the MIT License (see [License.txt]())

## List

* Item1
* Item2

## Table

Column1 | Column2
foo | bar
toto | tata

## Code

```hs
main :: IO ()
main = PutStrLn "Hello"
```
## Quote

>I never said half the crap people said i did
Albert einstein

## Image

![image](https://i2.kym-cdn.com/photos/images/facebook/001/228/324/4a4.gif)