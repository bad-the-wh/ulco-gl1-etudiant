/// \mainpage Documentation de drunk_player
///
/// \brief drunk_player est un système de lecture de jeux vidéos qui a trop bu. Il lit les vidéos contenues dans un dossier par morceaux, aléatoirement et parfois transformant l'image.
///
/// drunk_player utilise la bibliothèque de traitement d'image OpenCv et est composé:
///
///-d'une bibliothèque(drunk_player) contenant le code de base
///-d'un programme graphique(drunk_player_gui) qui affiche le résultat à l'écran 
///-d'un programme de console (drunk_player_cli) qui sort le résultat dans un fichier