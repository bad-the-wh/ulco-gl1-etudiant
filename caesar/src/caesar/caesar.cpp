#include <iostream>
#include <cstring>
#include <vector>

#include <caesar/caesar.hpp>

using namespace std;

string decaler(string phrase, int decalage){
    char a;
    for(int i=0; i<phrase.length(); i++){
        if(i+decalage<phrase.length()){
            a=phrase[i+decalage];
            phrase[i+decalage]=phrase[i];
            phrase[i]=a;
        }
        else{
            a=phrase[i+decalage-phrase.length()];
            phrase[i+decalage-phrase.length()]=phrase[i];
            phrase[i]=a;
        }    
    }
    return phrase;
}

string chiffrer(string phrase, int cle){
    string alphabet= "abcdefghijklmnopqrstuvwxyz";
    string alphabet2= decaler(alphabet, cle);
    for(int i=0; i<phrase.length(); i++){
        for(int j=0; j<alphabet.length(); j++){
            if(phrase[i]==alphabet[j]){
                phrase[i]=alphabet2[j];
            }
        }
    }
    return phrase;
}

vector<int> CompterOccurences(string phrase){
    string alphabet="abcdefghijklmnopqrstuvwxyz";
    vector<int> occ;
    for(int i=0; i<phrase.length(); i++){
        for(int j=0; j<alphabet.length(); j++){
            if(phrase[i]==alphabet[j]){
                occ[j]++;
            }
        }
    }
    return occ;
}

vector<double> CalculerFrequences(string phrase){
    vector<int> occ=CompterOccurences(phrase);
    int n=phrase.length();
    vector<double> freq;
    for(int i=0; i<26; i++){
        freq[i]=occ[i]*100/n;
    }
    return freq;
}

double CalculKhi(string phrase){
    vector<double> F {0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07, 0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06, 0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001};
    vector<double> f = CalculerFrequences(phrase);
    double khi;
    for(int i=0; i<26; i++){
        khi= khi + (f[i] - F[i])*(f[i] - F[i]) / F[i];
    }
    return khi;
}

int casserCesar(string phrase){
    double khi = CalculKhi(phrase);
    double khimin;
    int cle;
    for (int i=0; i<26; i++){
       string result= chiffrer(phrase, i);
       khimin = CalculKhi(result);
       if(khimin<khi){
           khi=khimin;
           cle=i;
       }
    }
    return cle;
}