#pragma once

#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

string decaler(string phrase, int decalage);

string chiffrer(string phrase, int cle);

vector<int> CompterOccurences(string phrase);

vector<double> CalculerFrequences(string phrase);

double CalculKhi(string phrase);

int casserCesar(string phrase);