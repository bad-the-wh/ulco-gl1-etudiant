#include <guess/guess.hpp>

#include <iostream>
using namespace std;

int main() {
    game game1;
    int number;
    int i = 5;
    cout << "Bienvenue au jeu de guess." << endl;
    while ((i!=0) || (number == game1.gnumber)){
        cout << "Entrez un nombre: " << endl;
        cin >> number;
        game1.devine(number);
        i--;
    }
    if(i>0){
        cout << "You Win!" << endl;
    }
    else{
        cout << "You Lose!" << endl;
    }
    return 0;
}

