#include <guess/guess.hpp>

#include <catch2/catch.hpp>

game game1;
int number=game1.gnumber;

TEST_CASE( "test 1" ) {
    REQUIRE( inserer(1) == true );
}

TEST_CASE( "test 3" ) {
    REQUIRE( game1.devine(number-1) == false );
}

TEST_CASE( "test 4" ) {
    REQUIRE( game1.devine(number+1) == false );
}

TEST_CASE( "test 5" ) {
    REQUIRE( game1.indice(number-1) == 'L' );
}

TEST_CASE( "test 6" ) {
    REQUIRE( game1.indice(number+1) == 'H' );
}

TEST_CASE( "test 7" ) {
    REQUIRE( game1.indice(number) == 'P' );
}

TEST_CASE( "test 8" ) {
    REQUIRE( game1.devine(number) == true );
}