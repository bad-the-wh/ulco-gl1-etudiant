#include <guess/guess.hpp>
#include <iostream>
#include <stdlib.h> //pour utiliser rand()
#include <time.h>

using namespace std;

///Le but du jeu est de trouver en 5 essais un nombre initialisé au hasard entre 1 et 100
///Classe Game qui va permettre d'initialiser un jeu
game::game(){///Surcharge du constructeur pour permettre une initialisation au hasard du nombre
    gnumber=(rand() % 100) + 1;///Numéro préalablement initialisé au hasard dès la construction de la classe
}

///Methode permettant de savoir si le nombre entré par l'utilisateur est le nombre défini au départ à la construction de la classe.
///Returne vrai si le nombre de l'utilisateur est égal à celui de la classe jeu et faux sinon
bool game::devine(int number){
    if(number == gnumber){
        return true;
    }
    else{
        return false;
    }
}


///Fonction vérifiant si le nombre entré par l'utilisateur existe
bool inserer(int number){
    if (number) {
        return true;
    }
    else {
        return false;
    }
}

///Methode permettant de donner des indices sur le nombre du jeu par rapport au nombre entré par l'utilisateur.
///retourne un caractère et un cout pour signifier si le nombre entré est trop grand(H), trop petit(L), ou est égal(P)
char game::indice(int number){
    if(number > gnumber){
        cout << "Too high" << endl;
        return 'H';
    }
    else if(number < gnumber){
        cout << "Too low" << endl;
        return 'L';
    }
    else if(number == gnumber){
        cout << "Perfect!" << endl;
        return 'P';
    }
}